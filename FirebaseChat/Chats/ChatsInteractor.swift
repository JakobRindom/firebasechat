//
//  ChatsInteractor.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit
import FirebaseFirestoreSwift
import FirebaseCore
import FirebaseFirestore
import FirebaseAuth

protocol ChatsBusinessLogic {
    func getChatRooms(request: Chats.ChatRooms.Request)
    func didSelectChatRoom(request: Chats.SelectChat.Request) 
    func checkLoggedIn(request: Chats.LoginStatus.Request)
    func LogOut(request: Chats.LogOut.Request)
    func createNewTopic(request: Chats.newTopic.Request)
    
    
}

protocol ChatsDataStore {
    var selectedChatRoom: DocumentSnapshot? { get set }
    var chatsDelegate: ChatsDelegate? { get set }
}

class ChatsInteractor: ChatsBusinessLogic, ChatsDataStore {
    
    var presenter: ChatsPresentationLogic?
    var localCollection: LocalCollection<ChatTopicModel>?
    var selectedChatRoom: DocumentSnapshot?
    var chatsDelegate: ChatsDelegate?
    
    func getChatRooms(request: Chats.ChatRooms.Request) {
        
        let query = Firestore.firestore().collection("ChatTopicModel")
        localCollection = LocalCollection(query: query) { [unowned self] (changes) in
            
            var indexPaths: [IndexPath] = []
            
            // Only care about additions in this block, updating existing reviews probably not important
            // as there's no way to edit reviews.
            for addition in changes.filter({ $0.type == .added }) {
                if let index = self.localCollection?.index(of: addition.document) {
                    let indexPath = IndexPath(row: index, section: 0)
                    indexPaths.append(indexPath)
                }
            }
            let response = Chats.ChatRooms.Response(chatRoomModels: localCollection, indexPaths: indexPaths)
            presenter?.presentChatRooms(response: response)
            
        }
        localCollection?.listen()
    }
    
    func didSelectChatRoom(request: Chats.SelectChat.Request) {
        self.selectedChatRoom = request.selectedChatRoom
        let response = Chats.SelectChat.Response()
        presenter?.presentSelectdChatRoom(response: response)
    }
    
    
    func checkLoggedIn(request: Chats.LoginStatus.Request) {
        if ((Auth.auth().currentUser?.uid) != nil) {
            let response = Chats.LoginStatus.Response(isLoggedIn: true)
            presenter?.presentLoginStatus(response: response)
        } else {
            self.chatsDelegate = request.chatsDelegate
            let response = Chats.LoginStatus.Response(isLoggedIn: false)
            presenter?.presentLoginStatus(response: response)
        }
    }
    
    func LogOut(request: Chats.LogOut.Request) {
        try? Auth.auth().signOut()
        self.localCollection?.stopListening()
        self.localCollection = nil
        let response = Chats.LogOut.Response()
        self.presenter?.didLogOut(response: response)
    }
    
    func createNewTopic(request: Chats.newTopic.Request) {
        let collection = Firestore.firestore().collection("ChatTopicModel")
        
        let newTopic = ChatTopicModel(chatTitle: request.topic)
        
        collection.addDocument(data: newTopic.dictionary) { error in
            if error == nil {
                let response = Chats.newTopic.Response()
                self.presenter?.didAddTopic(response: response)
            }
        }
    }
    
}
