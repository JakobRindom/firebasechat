//
//  ChatsModels.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit
import Firebase

enum Chats {
    // MARK: Use cases
    
    enum ChatRooms {
        struct Request {
        }
        struct Response {
            var chatRoomModels: LocalCollection<ChatTopicModel>!
            var indexPaths: [IndexPath]
        }
        struct ViewModel {
            var chatModels: LocalCollection<ChatTopicModel>!
            var indexPaths: [IndexPath]
        }
    }
    
    enum SelectChat {
        struct Request {
            var selectedChatRoom: DocumentSnapshot
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    enum LoginStatus {
        struct Request {
            var chatsDelegate: ChatsDelegate
        }
        struct Response {
            var isLoggedIn: Bool
        }
        struct ViewModel {
            var isLoggedIn: Bool
        }
    }
    
    enum LogOut {
        struct Request {
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    enum newTopic {
        struct Request {
            var topic: String
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
}
