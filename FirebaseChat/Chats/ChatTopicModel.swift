//
//  ChatTopicModel.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
//

import Foundation
import Firebase

struct ChatTopicModel {
    var chatTitle: String
    
    var dictionary: [String: Any] {
        return [
            "chatTitle": chatTitle,
        ]
    }
}

extension ChatTopicModel: DocumentSerializable {
    
    init?(dictionary: [String : Any]) {
        guard let chatTitle = dictionary["chatTitle"] as? String else { return nil }
        
        self.init(chatTitle: chatTitle)
    }
    
}
