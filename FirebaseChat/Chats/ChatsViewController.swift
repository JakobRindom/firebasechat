//
//  ChatsViewController.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit

protocol ChatsDisplayLogic: class {
    func displayChatRooms(viewModel: Chats.ChatRooms.ViewModel)
    func displayLoginStatus(viewModel: Chats.LoginStatus.ViewModel)
    func displaySelectedChat(viewModel: Chats.SelectChat.ViewModel)
    func didLogOut(viewModel: Chats.LogOut.ViewModel)
    func didAddNewTopic(viewModel: Chats.newTopic.ViewModel)
}

protocol ChatsDelegate {
    func didLogin()
}

class ChatsViewController: UIViewController, ChatsDisplayLogic, ChatsDelegate {
    var interactor: ChatsBusinessLogic?
    var router: (NSObjectProtocol & ChatsRoutingLogic & ChatsDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ChatsInteractor()
        let presenter = ChatsPresenter()
        let router = ChatsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    @IBOutlet weak var tableViewChats: UITableView!
    
    var arrChats: LocalCollection<ChatTopicModel>?
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkLogin()
    }
    
    
    @objc func logOut() {
        let request =  Chats.LogOut.Request()
        interactor?.LogOut(request: request)
    }
    
    @objc func addTopic() {
        let alertController = UIAlertController(title: "Add New Topic", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter topic Name"
        }
        let saveAction = UIAlertAction(title: "Save", style: .default , handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            if let topicTitle = textField.text, !topicTitle.isEmpty {
                let request = Chats.newTopic.Request(topic: textField.text ?? "unknown")
                self.interactor?.createNewTopic(request: request)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default , handler: nil)
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setupTableView()  {
        self.tableViewChats.delegate = self
        self.tableViewChats.dataSource = self
        self.tableViewChats.register(UINib(nibName: "ChatsCell", bundle: nil) , forCellReuseIdentifier: "ChatsCell")
    }
    
    func checkLogin()  {
        let request = Chats.LoginStatus.Request(chatsDelegate: self)
        interactor?.checkLoggedIn(request: request)
    }
    
    func didLogOut(viewModel: Chats.LogOut.ViewModel) {
        self.arrChats?.stopListening()
        self.arrChats = nil
        self.tableViewChats.reloadData()
        self.checkLogin()
    }
    
    func getAllMessages() {
        let request = Chats.ChatRooms.Request()
        interactor?.getChatRooms(request: request)
    }
    
    func displayChatRooms(viewModel: Chats.ChatRooms.ViewModel) {
        self.arrChats = viewModel.chatModels
        self.tableViewChats.insertRows(at: viewModel.indexPaths, with: .automatic)
    }
    
    func displayLoginStatus(viewModel: Chats.LoginStatus.ViewModel) {
        self.ToggleBarButtons(isLoggedIn: viewModel.isLoggedIn)
        if viewModel.isLoggedIn {
            if self.arrChats == nil {
                getAllMessages()
            }
        } else {
            self.arrChats = nil
            self.routeToLogin()
        }
    }
    
    
    
    @objc func routeToLogin() {
        router?.routeToLogin()
    }
    
    func displaySelectedChat(viewModel: Chats.SelectChat.ViewModel) {
        self.router?.routeToChat()
    }
    
    func didLogin() {
        self.checkLogin()
    }
    
    func didAddNewTopic(viewModel: Chats.newTopic.ViewModel) { }
    
    
    func ToggleBarButtons(isLoggedIn: Bool) {
        if isLoggedIn {
            let logOutBtn = UIBarButtonItem(
                title: "LogOut",
                style: .plain,
                target: self,
                action: #selector(logOut)
            )
            navigationItem.rightBarButtonItem = logOutBtn
            let addTopicBtn = UIBarButtonItem(
                title: "Add Topic",
                style: .plain,
                target: self,
                action: #selector(addTopic)
            )
            navigationItem.leftBarButtonItem = addTopicBtn
            
        } else {
            self.arrChats = nil
            let logInBtn = UIBarButtonItem(
                title: "LogIn",
                style: .plain,
                target: self,
                action: #selector(routeToLogin)
            )
            navigationItem.rightBarButtonItem = logInBtn
            navigationItem.leftBarButtonItem = nil
        }
    }
}

extension ChatsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = arrChats else {
            return 0
        }
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChatsCell") as? ChatsCell ,let data = self.arrChats else { return UITableViewCell()
        }
        
        let model = data[indexPath.row]
        cell.setupCell(with: model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let data = self.arrChats else { return}
        
        let model = data.documents[indexPath.row]
        let request = Chats.SelectChat.Request(selectedChatRoom: model )
        interactor?.didSelectChatRoom(request: request )
    }
    
}
