//
//  ChatsPresenter.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit

protocol ChatsPresentationLogic {
    func presentChatRooms(response: Chats.ChatRooms.Response)
    func presentSelectdChatRoom(response: Chats.SelectChat.Response)
    func presentLoginStatus(response: Chats.LoginStatus.Response)
    func didLogOut(response: Chats.LogOut.Response)
    func didAddTopic(response: Chats.newTopic.Response)
}


class ChatsPresenter: ChatsPresentationLogic {
    weak var viewController: ChatsDisplayLogic?
    
    func presentChatRooms(response: Chats.ChatRooms.Response) {
        let viewModel = Chats.ChatRooms.ViewModel(chatModels: response.chatRoomModels, indexPaths: response.indexPaths)
        viewController?.displayChatRooms(viewModel: viewModel)
    }
    
    func presentLoginStatus(response: Chats.LoginStatus.Response) {
        let viewModel = Chats.LoginStatus.ViewModel(isLoggedIn: response.isLoggedIn)
        viewController?.displayLoginStatus(viewModel: viewModel)
    }
    
    func didLogOut(response: Chats.LogOut.Response) {
        let viewModel = Chats.LogOut.ViewModel()
        viewController?.didLogOut(viewModel: viewModel)
    }
    
    func presentSelectdChatRoom(response: Chats.SelectChat.Response) {
        let viewModel = Chats.SelectChat.ViewModel()
        viewController?.displaySelectedChat(viewModel: viewModel)
    }
    
    func didAddTopic(response: Chats.newTopic.Response) {
        let viewModel = Chats.newTopic.ViewModel()
        viewController?.didAddNewTopic(viewModel: viewModel)
    }
}
