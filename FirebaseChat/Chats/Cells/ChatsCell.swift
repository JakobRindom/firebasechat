//
//  ChatsCell.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
//

import UIKit

class ChatsCell: UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(with data: ChatTopicModel) {
        self.lblUserName.text = data.chatTitle
    }

    
}
