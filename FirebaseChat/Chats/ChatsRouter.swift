//
//  ChatsRouter.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit

@objc protocol ChatsRoutingLogic {
    func routeToChat()
    func routeToLogin()
}

protocol ChatsDataPassing {
    var dataStore: ChatsDataStore? { get }
}

class ChatsRouter: NSObject, ChatsRoutingLogic, ChatsDataPassing {
    weak var viewController: ChatsViewController?
    var dataStore: ChatsDataStore?
    
    // MARK: Routing
    
    func routeToChat() {

        let storyboard = UIStoryboard(name: "ChatRoom", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "ChatRoomViewController") as! ChatRoomViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToChatRoom(source: dataStore!, destination: &destinationDS)
        navigateToChatRoom(source: viewController!, destination: destinationVC)
    }
    
    func routeToLogin() {

        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToLogin(source: dataStore!, destination: &destinationDS)
        navigateToLogin(source: viewController!, destination: destinationVC)
    }
    
    // MARK: Navigation
    
    func navigateToChatRoom(source: ChatsViewController, destination: ChatRoomViewController) {
      source.show(destination, sender: nil)
    }
    
    func navigateToLogin(source: ChatsViewController, destination: LoginViewController) {
        if #available(iOS 13.0, *) {
            source.isModalInPresentation = true
        }
        source.present(destination, animated: true)
    }
    
    // MARK: Passing data
    
    func passDataToChatRoom(source: ChatsDataStore, destination: inout ChatRoomDataStore) {
      destination.selectedChatRoom = source.selectedChatRoom
    }
    func passDataToLogin(source: ChatsDataStore, destination: inout LoginDataStore) {
        destination.chatDelegate = source.chatsDelegate
    }
}
