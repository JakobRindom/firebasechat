//
//  Date+Extensions.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
//

import Foundation

extension Date {
    
    func asString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "da_DK")
        dateFormatter.doesRelativeDateFormatting = true
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .short
        return dateFormatter.string(from: self)
        
    }
    
}
