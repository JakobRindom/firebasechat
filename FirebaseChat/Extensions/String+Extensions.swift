//
//  String+Extensions.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
//

import Foundation
import UIKit

extension String {
    
    /// Generates initaias for a person. It only returns two characters: The first character in first name and the first character in last surname
    func getInitialsForName() -> String {
        return self.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + ($1 == "" ? "" : "\($1.first!)") }
    }
    
    /// Calculates the height of a text, where you know the width of the text-box.
    ///
    /// - Parameters:
    ///   - width: The width of the text-box
    ///   - font: The font used in the text-box
    /// - Returns: The calculated height of the text with the given constraints and font.
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height.rounded(.up)
    }
}
