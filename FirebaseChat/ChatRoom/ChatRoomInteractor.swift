//
//  ChatRoomInteractor.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
// 

import UIKit
import Firebase
import FirebaseAuth

protocol ChatRoomBusinessLogic {
    func getMessages(request: ChatRoom.ChatMessages.Request)
    func sendMessage(request: ChatRoom.SendMessage.Request)
}

protocol ChatRoomDataStore {
    var selectedChatRoom: DocumentSnapshot? { get set }
}

class ChatRoomInteractor: ChatRoomBusinessLogic, ChatRoomDataStore {
    var presenter: ChatRoomPresentationLogic?
    var selectedChatRoom: DocumentSnapshot?
    
    var localCollection: LocalCollection<MessageModel>!
    
    // MARK: Get Messages
    
    func getMessages(request: ChatRoom.ChatMessages.Request) {
   
        guard let selectedChatRoom = selectedChatRoom else {
            return
        }
        
        let query = selectedChatRoom.reference.collection("MessageModel") .order(by: "sendDate")
        localCollection = LocalCollection(query: query) { [unowned self] (changes) in
            
            var indexPaths: [IndexPath] = []
            
            // Only care about additions in this block, updating existing reviews probably not important
            // as there's no way to edit reviews.
            for addition in changes.filter({ $0.type == .added }) {
                let index = self.localCollection.index(of: addition.document)!
                let indexPath = IndexPath(row: index, section: 0)
                indexPaths.append(indexPath)
            }
            let data = selectedChatRoom.data()
            if let title = data?["chatTitle"] as? String {
            
            let response = ChatRoom.ChatMessages.Response(messageModels: localCollection, indexPaths: indexPaths, title: title)
            presenter?.presentMessages(response: response)
            }
        }
        localCollection.listen()
    }
    
    func sendMessage(request: ChatRoom.SendMessage.Request) {
        let messageRef = self.selectedChatRoom?.reference.collection("MessageModel")
        
        let message = MessageModel(senderName: Auth.auth().currentUser?.displayName ?? "Unknown" , senderId: Auth.auth().currentUser?.uid ?? "0" , message: request.message , sendDate: Date())
        
        messageRef?.addDocument(data: message.dictionary, completion: { error in
            let response = ChatRoom.SendMessage.Response()
            self.presenter?.messageSend(response: response)
        })
    }
    
}
