//
//  ChatRoomRouter.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
// 

import UIKit

@objc protocol ChatRoomRoutingLogic {
}

protocol ChatRoomDataPassing {
    var dataStore: ChatRoomDataStore? { get }
}

class ChatRoomRouter: NSObject, ChatRoomRoutingLogic, ChatRoomDataPassing {
    weak var viewController: ChatRoomViewController?
    var dataStore: ChatRoomDataStore?
    
}
