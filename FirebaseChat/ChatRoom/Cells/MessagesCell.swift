//
//  MyMessagesCell.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
//

import UIKit

class MessagesCell: UITableViewCell {

    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var viewInitialsBackground: UIView!
    @IBOutlet weak var lblInitials: UILabel!
    @IBOutlet weak var viewMessageBackground: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblSendDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewMessageBackground.layer.cornerRadius = 10
        self.viewInitialsBackground.layer.cornerRadius = 15
    }
    
    func setupCell(with data: MessageModel) {
        self.lblInitials.text = data.senderName.getInitialsForName()
        self.lblMessage.text = data.message
        self.lblSenderName.text = data.senderName
        self.lblSendDate.text = data.sendDate.asString()
    }

}
