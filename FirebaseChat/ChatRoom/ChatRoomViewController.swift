//
//  ChatRoomViewController.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
// 

import UIKit

protocol ChatRoomDisplayLogic: class {
    func displayMessages(viewModel: ChatRoom.ChatMessages.ViewModel)
    func messageWasSend(viewModel: ChatRoom.SendMessage.ViewModel)
}

class ChatRoomViewController: UIViewController, ChatRoomDisplayLogic {
    var interactor: ChatRoomBusinessLogic?
    var router: (NSObjectProtocol & ChatRoomRoutingLogic & ChatRoomDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ChatRoomInteractor()
        let presenter = ChatRoomPresenter()
        let router = ChatRoomRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    @IBOutlet weak var tableViewMessages: UITableView!
    @IBOutlet weak var constrintMessageHeight: NSLayoutConstraint!
    @IBOutlet weak var txtViewMessage: UITextView!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var constraintInputViewBottom: NSLayoutConstraint!
    
    var arrMessages: LocalCollection<MessageModel>?
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        getMessages()
        
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardDidChangeAppearance(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardDidChangeAppearance(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tapGesture)
        //tapGesture.cancelsTouchesInView = false
        
        self.txtViewMessage.delegate = self
        self.txtViewMessage.layer.cornerRadius = 10
        self.txtViewMessage.text = ""
    }
    
    
    func setupTableView()  {
        self.tableViewMessages.delegate = self
        self.tableViewMessages.dataSource = self
        self.tableViewMessages.allowsSelection = false
        self.tableViewMessages.register(UINib(nibName: "OtherMessagesCell", bundle: nil) , forCellReuseIdentifier: "OtherMessagesCell")
        self.tableViewMessages.register(UINib(nibName: "MyMessagesCell", bundle: nil) , forCellReuseIdentifier: "MyMessagesCell")
    }
    
    @IBAction func btnSendMessage(_ sender: Any) {
        let request = ChatRoom.SendMessage.Request(message: self.txtViewMessage.text)
        self.interactor?.sendMessage(request: request)
    }
    
    
    func getMessages() {
        let request = ChatRoom.ChatMessages.Request()
        interactor?.getMessages(request: request)
    }
    
    func displayMessages(viewModel: ChatRoom.ChatMessages.ViewModel) {
        self.title = viewModel.title
        self.arrMessages = viewModel.messageModels
        self.tableViewMessages.insertRows(at: viewModel.indexPaths, with: .automatic)
        if arrMessages!.count != 0 {
            self.tableViewMessages.scrollToRow(at: IndexPath(row: arrMessages!.count-1 , section: 0) , at: .bottom, animated: false)
        }
    }
    
    func messageWasSend(viewModel: ChatRoom.SendMessage.ViewModel) {
        self.txtViewMessage.text = ""
        self.tableViewMessages.scrollToRow(at: IndexPath(row: arrMessages!.count-1 , section: 0) , at: .bottom, animated: true)
    }
    
    
    @objc func keyboardDidChangeAppearance(_ notification: Notification) {
        // Keyboard will appear
        if UIResponder.keyboardWillChangeFrameNotification == notification.name {
            // Calculate keyboard height
            let keyboardHeight = self.getKeyboardHeightFor(notification: notification)
            self.constraintInputViewBottom.constant += keyboardHeight
            
        } else if UIResponder.keyboardWillHideNotification == notification.name {
            self.constraintInputViewBottom.constant = 0
        }
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func getKeyboardHeightFor(notification: Notification) -> CGFloat {
        let begin = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let end = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        return begin.origin.y - end.origin.y
    }
    
}

extension ChatRoomViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let messages = self.arrMessages {
            return messages.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let data = self.arrMessages else { return UITableViewCell() }
        let model = data[indexPath.row]
        
        if model.isFromCurrentUser {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyMessagesCell") as? MessagesCell  else { return UITableViewCell() }
            
            cell.setupCell(with: model)
            
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "OtherMessagesCell") as? MessagesCell  else { return UITableViewCell() }
            cell.setupCell(with: model)
            
            return cell
        }
    }
    
}

extension ChatRoomViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        let textHeight =  textView.text.height(withConstrainedWidth: self.txtViewMessage.bounds.width, font: self.txtViewMessage.font!)
        
        let maxSize: Float = 250
        let minSize: Float = 30
        
        let ranged = min(max(minSize, Float(textHeight)), maxSize)
        self.constrintMessageHeight.constant = CGFloat(ranged) + 10
    }
    
}
