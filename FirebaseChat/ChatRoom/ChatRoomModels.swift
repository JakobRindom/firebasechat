//
//  ChatRoomModels.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
// 

import UIKit

enum ChatRoom {
    // MARK: Use cases
    
    enum ChatMessages {
        struct Request {
        }
        struct Response {
            var messageModels: LocalCollection<MessageModel>!
            var indexPaths: [IndexPath]
            var title: String
        }
        struct ViewModel {
            var messageModels: LocalCollection<MessageModel>!
            var indexPaths: [IndexPath]
            var title: String
        }
    }
    
    enum SendMessage {
        struct Request {
            var message: String
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
}
