//
//  ChatRoomPresenter.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
// 

import UIKit

protocol ChatRoomPresentationLogic {
    func presentMessages(response: ChatRoom.ChatMessages.Response)
    func messageSend(response: ChatRoom.SendMessage.Response)
}

class ChatRoomPresenter: ChatRoomPresentationLogic {
    weak var viewController: ChatRoomDisplayLogic?

    func presentMessages(response: ChatRoom.ChatMessages.Response) {
        let viewModel = ChatRoom.ChatMessages.ViewModel(messageModels: response.messageModels, indexPaths: response.indexPaths, title: response.title)
        viewController?.displayMessages(viewModel: viewModel)
    }
    
    func messageSend(response: ChatRoom.SendMessage.Response) {
        let viewModel = ChatRoom.SendMessage.ViewModel()
        viewController?.messageWasSend(viewModel: viewModel)
    }

}
