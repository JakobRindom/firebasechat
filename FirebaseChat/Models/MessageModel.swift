//
//  MessagesModel.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 08/02/2021.
//

import Foundation
import Firebase
import FirebaseAuth

struct MessageModel {
    var senderName: String
    var senderId: String
    var message: String
    var sendDate: Date
    
    var dictionary: [String: Any] {
        return [
            "sendDate": Timestamp(date: sendDate),
            "senderName": senderName,
            "senderId": senderId,
            "message": message
        ]
    }
    
    var isFromCurrentUser: Bool {
        get {
            return senderId == Auth.auth().currentUser?.uid
        }
    }
}

extension MessageModel: DocumentSerializable {
    
    init?(dictionary: [String : Any]) {
        guard let senderName = dictionary["senderName"] as? String,
              let senderId = dictionary["senderId"] as? String,
              let message = dictionary["message"] as? String,
              let sendDate = dictionary["sendDate"] as? Timestamp
        else { return nil }
        
        self.init(senderName: senderName, senderId: senderId, message: message, sendDate: sendDate.dateValue())
        
    }
    
}
