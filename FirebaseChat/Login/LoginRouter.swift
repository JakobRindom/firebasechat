//
//  LoginRouter.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit

@objc protocol LoginRoutingLogic {
}

protocol LoginDataPassing {
    var dataStore: LoginDataStore? { get }
}

class LoginRouter: NSObject, LoginRoutingLogic, LoginDataPassing {
    weak var viewController: LoginViewController?
    var dataStore: LoginDataStore?
    
  
}
