//
//  LoginPresenter.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
//

import UIKit

protocol LoginPresentationLogic {
    func userCreated(response: Login.CreateUser.Response)
    func userSignedIn(response: Login.SignIn.Response)
}

class LoginPresenter: LoginPresentationLogic {
    weak var viewController: LoginDisplayLogic?
    
    // MARK: Do something
    
    func userCreated(response: Login.CreateUser.Response) {
        let viewModel = Login.CreateUser.ViewModel()
        viewController?.userCreated(viewModel: viewModel)
    }
    
    func userSignedIn(response: Login.SignIn.Response) {
        let viewModel = Login.SignIn.ViewModel()
        viewController?.userSignedIn(viewModel: viewModel)
    }
}
