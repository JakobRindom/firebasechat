//
//  LoginViewController.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit

protocol LoginDisplayLogic: class {
    func userCreated(viewModel: Login.CreateUser.ViewModel)
    func userSignedIn(viewModel: Login.SignIn.ViewModel)
}

class LoginViewController: UIViewController, LoginDisplayLogic {
    var interactor: LoginBusinessLogic?
    var router: (NSObjectProtocol & LoginRoutingLogic & LoginDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = LoginInteractor()
        let presenter = LoginPresenter()
        let router = LoginRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    @IBOutlet weak var txtFieldUserName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var btnLoginRegister: UIButton!
    @IBOutlet weak var constraintKeyboardView: NSLayoutConstraint!
    
    var isLoginMode: Bool {
        return segmentedControl.selectedSegmentIndex == 0
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtFieldUserName.isHidden = true
        self.txtFieldEmail.placeholder = "E-mail"
        self.txtFieldPassword.placeholder = "Password"
        self.txtFieldUserName.placeholder = "Username"
        
        self.segmentedControl.setTitle("Login", forSegmentAt: 0)
        self.segmentedControl.setTitle("SignUp", forSegmentAt: 1)
        self.btnLoginRegister.setTitle(self.isLoginMode ? "Login" : "Register", for: .normal)
        
        setupKeyboard()
    }
    
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardDidChangeAppearance(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardDidChangeAppearance(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tapGesture)
        //tapGesture.cancelsTouchesInView = false
    }
    
    // MARK: Do something
    @IBAction func btnLoginRegister(_ sender: Any) {
        guard let email = self.txtFieldEmail.text, let password = self.txtFieldPassword.text else {
            //TODO: show alert
            return
        }
        
        if self.isLoginMode {
            let request = Login.SignIn.Request(email: email, password: password)
            interactor?.signIn(request: request)
        } else {
            guard let userName = self.txtFieldUserName.text else { return }
            let request = Login.CreateUser.Request(username: userName, email: email, password: password)
            interactor?.createUser(request: request)
        }
        
    }
    
    @IBAction func segmentedChanged(_ sender: UISegmentedControl) {
        UIView.animate(withDuration: 0.5) {
            self.txtFieldUserName.isHidden = self.isLoginMode
            self.btnLoginRegister.setTitle(self.isLoginMode ? "Login" : "Register", for: .normal)
        }
    }
    
    func userCreated(viewModel: Login.CreateUser.ViewModel) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func userSignedIn(viewModel: Login.SignIn.ViewModel) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func keyboardDidChangeAppearance(_ notification: Notification) {
        // Keyboard will appear
        if UIResponder.keyboardWillChangeFrameNotification == notification.name {
            // Calculate keyboard height
            let keyboardHeight = self.getKeyboardHeightFor(notification: notification)
            self.constraintKeyboardView.constant += keyboardHeight
            
        } else if UIResponder.keyboardWillHideNotification == notification.name {
            self.constraintKeyboardView.constant = 0
        }
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func getKeyboardHeightFor(notification: Notification) -> CGFloat {
        let begin = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let end = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        return begin.origin.y - end.origin.y
    }
    
}
