//
//  LoginInteractor.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit
import FirebaseAuth
import FirebaseFirestoreSwift

protocol LoginBusinessLogic {
    func createUser(request: Login.CreateUser.Request)
    func signIn(request: Login.SignIn.Request)
}

protocol LoginDataStore {
    var chatDelegate: ChatsDelegate? { get set }
}

class LoginInteractor: LoginBusinessLogic, LoginDataStore {
    var presenter: LoginPresentationLogic?
    var chatDelegate: ChatsDelegate?
    
    
    func createUser(request: Login.CreateUser.Request) {
        
        Auth.auth().createUser(withEmail: request.email, password: request.password) { authResult, error in
            
            if let error = error {
                print(error.localizedDescription)
            } else {
                
                let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                changeRequest?.displayName = request.username
                changeRequest?.commitChanges { (error) in
                }
                self.chatDelegate?.didLogin()
                let response = Login.CreateUser.Response()
                self.presenter?.userCreated(response: response)
            }
        }
    }
    
    func signIn(request: Login.SignIn.Request) {
        Auth.auth().signIn(withEmail: request.email, password: request.password) { (result, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                self.chatDelegate?.didLogin()
                let response = Login.SignIn.Response()
                self.presenter?.userSignedIn(response: response)
            }
        }
    }
}
