//
//  LoginModels.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
// 

import UIKit

enum Login {
    // MARK: Use cases
    
    enum CreateUser {
        struct Request {
            var username: String
            var email: String
            var password: String
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    enum SignIn {
        struct Request {
            var email: String
            var password: String
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    
}
