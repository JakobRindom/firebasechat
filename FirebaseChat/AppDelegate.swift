//
//  AppDelegate.swift
//  FirebaseChat
//
//  Created by Jakob Rindom on 07/02/2021.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if let chats = UIStoryboard(name: "Chats", bundle: nil).instantiateInitialViewController() {
            let navigation = UINavigationController(rootViewController: chats)
            
            let frame = UIScreen.main.bounds
            window = UIWindow(frame: frame)
            if #available(iOS 13.0, *) {
                window?.overrideUserInterfaceStyle = .light
            }
            window!.rootViewController = navigation
            window!.makeKeyAndVisible()
        }
        
        FirebaseApp.configure()
        let db = Firestore.firestore()
        print(db)
        return true
    }
    
}

